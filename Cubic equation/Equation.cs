﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using Cubic_equation.CustomComplex;

namespace Cubic_equation
{
    public class Equation
    {
        private string cequation;
        private bool x3;
        private bool x2;
        private bool x;
        private bool isOk;

        public float A;
        public float B;
        public float C;
        public float D;

        public string CEquation
        {
            set { cequation = value; }
            get { return cequation + "=0"; }
        }
        public Equation()
        {
            A = 0;
            B = 0;
            C = 0;
            D = 0;
        }
        public void Parse(string equation)
        {
            equation = equation.Replace(" ", "").ToLower();
            CEquation = string.Copy(equation);
            equation += "=";
            try
            {
                A = 0; B = 0; C = 0; D = 0;
                x3 = false; x2 = false; x = false;
                isOk = true;
                Regex regex = new Regex(@"[0-9]+");
                Regex regexC = new Regex(@"[+-/*=]+");       // x233+3x34-30x30
                for (int i = 0; i < equation.Length - 1; i++)
                {
                    if (i + 2 < equation.Length)
                    {
                        if (equation[i] == 'x'
                            && !((equation[i + 1] == '2' && regexC.Match(equation[i + 2].ToString()).Success
                            || regexC.Match(equation[i + 1].ToString()).Success)
                            || (equation[i + 1] == '3' && regexC.Match(equation[i + 2].ToString()).Success
                            || regexC.Match(equation[i + 1].ToString()).Success)))
                        {
                            throw new Exception("Given invalid X");
                        }
                    }
                    if (equation[i].ToString() + equation[i + 1].ToString() == "x3")
                    {
                        int count = 0;
                        if (x3)
                        {
                            throw new Exception("X3 meets the second time");
                        }
                        x3 = true;
                        for (int j = i - 1; j >= 0; j--)
                        {
                            Match match = regex.Match(equation[j].ToString());
                            if (match.Success)
                            {
                                count++;
                            }
                            else
                                break;
                        }
                        if (count == 0)
                        {
                            if (i - 1 >= 0)
                            {
                                if (equation[i - count - 1] == '-')
                                {
                                    A = -1;
                                }
                                else
                                    A = 1;
                            }
                            else
                                A = 1;
                        }
                        else if (i - count - 1 >= 0)
                        {
                            if (equation[i - count - 1] == '-')
                            {
                                A = -float.Parse(equation.Substring(i - count, count));
                            }
                            else
                            {
                                A = float.Parse(equation.Substring(i - count, count));
                            }
                        }
                        else
                        {
                            A = float.Parse(equation.Substring(i - count, count));
                        }
                        i++;
                    }
                    else if (equation[i].ToString() + equation[i + 1].ToString() == "x2")
                    {
                        int count = 0;
                        if (x2)
                        {
                            throw new Exception("X2 meets the second time");
                        }
                        x2 = true;
                        for (int j = i - 1; j >= 0; j--)
                        {
                            Match match = regex.Match(equation[j].ToString());
                            if (match.Success)
                            {
                                count++;
                            }
                            else
                                break;
                        }
                        if(count == 0)
                        {
                            if (i - 1 >= 0)
                            {
                                if (equation[i - count - 1] == '-')
                                {
                                    B = -1;
                                }
                                else
                                    B = 1;
                            }
                            else
                                B = 1;
                        }
                        else if (i - count - 1 >= 0)
                        {
                            if (equation[i - count - 1] == '-')
                            {
                                B = -float.Parse(equation.Substring(i - count, count));
                            }
                            else
                            {
                                B = float.Parse(equation.Substring(i - count, count));
                            }
                        }
                        else
                        {
                            B = float.Parse(equation.Substring(i - count, count));
                        }
                        i++;
                    }
                    else if (equation[i] == 'x')
                    {
                        if (x)
                        {
                            throw new Exception("X meets the second time");
                        }
                        x = true;
                        int count = 0;
                        for (int j = i - 1; j >= 0; j--)                           
                        {                                                    
                            Match match = regex.Match(equation[j].ToString());
                            if (match.Success)
                            {
                                count++;
                            }
                            else
                                break;                                         
                        }
                        if (count == 0)
                        {
                            if (i - 1 >= 0)
                            {
                                if (equation[i - count - 1] == '-')
                                {
                                    C = -1;
                                }
                                else
                                    C = 1;
                            }
                            else
                                C = 1;
                        }
                        else if (i - count - 1 >= 0)
                        {
                            if (equation[i - count - 1] == '-')
                            {
                                C = -float.Parse(equation.Substring(i - count, count));
                            }
                            else
                            {
                                C = float.Parse(equation.Substring(i - count, count));
                            }
                        }
                        else
                        {
                            C = float.Parse(equation.Substring(i - count, count));
                        }
                    }
                    else if (regex.Match(equation[i].ToString()).Success)
                    {
                        for (int j = i; j < equation.Length; j++)
                        {
                            if(equation[j] != 'x')
                            {
                                if (regexC.Match(equation[j].ToString()).Success)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                goto endC;
                            }
                            
                        }
                        int count = 0;
                        for (int j = i; j < equation.Length; j++)
                        {
                            Match match = regex.Match(equation[j].ToString());
                            if (match.Success)
                            {
                                count++;
                            }
                            else
                                break;
                        }
                        if (i - 1 >= 0)
                        {
                            if (equation[i - 1] == '-')
                            {
                                D = -float.Parse(equation.Substring(i, count));
                            }
                            else
                            {
                                D = float.Parse(equation.Substring(i, count));
                            }
                        }
                        else
                        {
                            D = float.Parse(equation.Substring(i, count));
                        }
                        i += count;
                    endC:;
                    }
                }
                if(!x3)
                {
                    throw new Exception("X3 are missing");
                }
            }
            catch(Exception e)
            {
                isOk = false;
                MessageBox.Show("Было введино некоректное уровнение owO" + Environment.NewLine + e.Message);
            }
        }

        public string GetResult()
        {            
            if(A == 0 || !isOk)
            {                
                return "Не верное вырожение";
            }
            var roots = Roots(D, C, B, A);
            Complex root1 = roots.Item1;
            Complex root2 = roots.Item2;
            Complex root3 = roots.Item3;
            return "x1 = " + root1.ToString() + Environment.NewLine +
                   "x2 = " + root2.ToString() + Environment.NewLine +
                   "x3 = " + root3.ToString();
        }
        public static Tuple<Complex, Complex, Complex> Roots(double d, double c, double b, double a)
        {
            double A = b * b - 3 * a * c;
            double B = 2 * b * b * b - 9 * a * b * c + 27 * a * a * d;
            double s = -1 / (3 * a);
            double D = (B * B - 4 * A * A * A) / (-27 * a * a);
            if (D == 0d)
            {
                if (A == 0d)
                {
                    var u = new Complex(s * b, 0d);
                    return new Tuple<Complex, Complex, Complex>(u, u, u);
                }
                var v = new Complex((9 * a * d - b * c) / (2 * A), 0d);                
                var w = new Complex((4 * a * b * c - 9 * a * a * d - b * b * b) / (a * A), 0d);
                return new Tuple<Complex, Complex, Complex>(v, v, w);
            }
            var C = (A == 0)
                ? new Complex(B, 0d).CubicRoots()
                : ((B + Complex.Sqrt(B * B - 4 * A * A * A)) / 2).CubicRoots();
            
            return new Tuple<Complex, Complex, Complex>(
                s * (b + C.Item1 + A / C.Item1),
                s * (b + C.Item2 + A / C.Item2),
                s * (b + C.Item3 + A / C.Item3));
        }
    }
}