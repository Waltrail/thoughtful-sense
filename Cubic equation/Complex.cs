﻿using System;
using System.Globalization;

namespace Cubic_equation.CustomComplex
{
    public class Complex
    {
        public double Real { get; private set; }
        public double Imaginary { get; private set; }
        public double Magnitude
        {
            get
            {
                return Abs(this);
            }
        }
        public double Phase
        {
            get
            {
                return Math.Atan2(Imaginary, Real);
            }
        }
        public Complex(double real, double imaginary)
        {
            Real = real;
            Imaginary = imaginary;
        }
        public static Complex FromPolarCoordinates(double magnitude, double phase)
        {
            return new Complex((magnitude * Math.Cos(phase)), (magnitude * Math.Sin(phase)));
        }     
        public static Complex operator +(Complex left, Complex right)
        {
            return new Complex((left.Real + right.Real), (left.Imaginary + right.Imaginary));
        }
        public static Complex operator *(Complex left, Complex right)
        {
            double result_Realpart = (left.Real * right.Real) - (left.Imaginary * right.Imaginary);
            double result_Imaginarypart = (left.Imaginary * right.Real) + (left.Real * right.Imaginary);
            return (new Complex(result_Realpart, result_Imaginarypart));
        }
        public static Complex operator /(Complex left, Complex right)
        {
            double a = left.Real;
            double b = left.Imaginary;
            double c = right.Real;
            double d = right.Imaginary;
            if (Math.Abs(d) < Math.Abs(c))
            {
                double doc = d / c;
                return new Complex((a + b * doc) / (c + d * doc), (b - a * doc) / (c + d * doc));
            }
            else
            {
                double cod = c / d;
                return new Complex((b + a * cod) / (d + c * cod), (-a + b * cod) / (d + c * cod));
            }
        }
        public static double Abs(Complex value)
        {
            if (double.IsInfinity(value.Real) || double.IsInfinity(value.Imaginary))
            {
                return double.PositiveInfinity;
            }
            double c = Math.Abs(value.Real);
            double d = Math.Abs(value.Imaginary);
            if (c > d)
            {
                double r = d / c;
                return c * Math.Sqrt(1.0 + r * r);
            }
            else if (d == 0.0)
            {
                return c;
            }
            else
            {
                double r = c / d;
                return d * Math.Sqrt(1.0 + r * r);
            }
        }
        public static bool operator ==(Complex left, Complex right)
        {
            return (left.Real == right.Real) && (left.Imaginary == right.Imaginary);
        }
        public static bool operator !=(Complex left, Complex right)
        {
            return (left.Real != right.Real) || (left.Imaginary != right.Imaginary);
        }
        public static implicit operator Complex(double value)
        {
            return (new Complex(value, 0.0));
        }
        public static Complex Sqrt(Complex value)
        {
            return FromPolarCoordinates(Math.Sqrt(value.Magnitude), value.Phase / 2.0);
        }
        public override string ToString()
        {
            Imaginary = Math.Round(Imaginary, 10);
            Real = Math.Round(Real, 10);
            if (Imaginary == 0)
                return (string.Format(CultureInfo.CurrentCulture, "Реалиный: {0}", Real));
            return (string.Format(CultureInfo.CurrentCulture, "Реалиный: {0}, Мнимый: {1}", Real, Imaginary));
        }
        public Tuple<Complex, Complex, Complex> CubicRoots()
        {
            var r = Math.Pow(Magnitude, 1d / 3d);
            var theta = Phase / 3;
            const double shift = Math.PI * 2 / 3;
            return new Tuple<Complex, Complex, Complex>(
                FromPolarCoordinates(r, theta),
                FromPolarCoordinates(r, theta + shift),
                FromPolarCoordinates(r, theta - shift));
        }
    }
}
