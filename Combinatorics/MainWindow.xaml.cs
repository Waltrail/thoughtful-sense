﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Combinatorics
{
    public partial class MainWindow : Window
    {
        private List<Combinatoric> combinatorics;
        public MainWindow()
        {
            InitializeComponent();
            combinatorics = new List<Combinatoric>()
            {
                new PermutationsManager(),
                new AllocationManager(),
                new CombinationManager()
            };
            BtnGetResult.Click += GetResult;
            BtnIn.Click += Load;
            BtnOut.Click += Out;
        }

        private void Out(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";

            string result = "";
            foreach (var comb in combinatorics)
            {
                comb.Init(BoxN.Text, BoxM.Text);
                result += comb.GetResult() + ", ";
            }
            result = result.Substring(0, result.Length - 2);
            if (saveFileDialog.ShowDialog() == true)
                File.WriteAllText(saveFileDialog.FileName, "N" + combinatorics[1].N + " M" + combinatorics[1].M + Environment.NewLine + result);
        }

        private void Load(object sender, RoutedEventArgs e)
        {
            string combin = "";
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog.Multiselect = false;
            if (openFileDialog.ShowDialog() == true)
                combin = File.ReadAllText(openFileDialog.FileName);            
            if (combin.IndexOf('\n') != -1)
            {
                combin = combin.Substring(0, combin.IndexOf('\n'));
            }
            int N = -1;
            int M = -1;
            try
            {
                combin = combin.Replace(" ", "").ToLower();
                for (int i = 0; i < combin.Length; i++)
                {
                    if (combin[i] == 'n')
                    {
                        int count = 0;
                        for (int j = i + 1; j < combin.Length; j++)
                        {
                            if (combin[j] == 'm')
                            {
                                break;
                            }
                            else
                                count++;
                        }
                        N = int.Parse(combin.Substring(i + 1, count));
                        i += count;
                    }
                    else if (combin[i] == 'm')
                    {
                        int count = 0;
                        for (int j = i + 1; j < combin.Length; j++)
                        {
                            if (combin[j] == 'n')
                            {
                                break;
                            }
                            else
                                count++;
                        }
                        M = int.Parse(combin.Substring(i + 1, count));
                        i += count;
                    }
                }
                if(N == - 1 || M == -1)
                {
                    throw new Exception("N or M were not found");
                }
                BoxN.Text = N.ToString();
                BoxM.Text = M.ToString();
                string result = "";
                foreach (var comb in combinatorics)
                {
                    comb.Init(BoxN.Text, BoxM.Text);
                    result += comb.GetResult() + ", ";
                }
                result = result.Substring(0, result.Length - 2);
                BoxResult.Text = result;
            }
            catch(Exception ee)
            {
                MessageBox.Show("Ошибка загрузки файла" + Environment.NewLine + ee.Message);
            }

        }

        private void GetResult(object sender, RoutedEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            string result = "";
            foreach(var comb in combinatorics)
            {
                comb.Init(BoxN.Text, BoxM.Text);
                result += comb.GetResult() + ", ";
            }
            result = result.Substring(0, result.Length - 2);
            BoxResult.Text = result;
            sw.Stop();
            MessageBox.Show((sw.ElapsedMilliseconds / 1000.0).ToString());
        }
    }
}
