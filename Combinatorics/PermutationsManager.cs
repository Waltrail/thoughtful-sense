﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Combinatorics
{
    public class PermutationsManager : Combinatoric
    {
        public override void Init(string n, string m)
        {
            isOk = true;
            try
            {
                N = int.Parse(n);
            }
            catch (Exception e)
            {
                isOk = false;
                MessageBox.Show("Не верные данные" + Environment.NewLine + e.Message);
            }
        }
        public override string GetResult()
        {
            try
            {
                if (!isOk)
                {
                    return "";
                }
                float P = Factorial(N);
                return "P = " + P;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка при нахождении перестановки" + Environment.NewLine + e.Message);
                return "P != ";
            }
        }
    }
}
