﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Combinatorics
{
    public class AllocationManager : Combinatoric
    {
        public override void Init(string n, string m)
        {
            isOk = true;
            try
            {
                N = int.Parse(n);
                M = int.Parse(m);
            }
            catch (Exception e)
            {
                isOk = false;
                MessageBox.Show("Не верные данные" + Environment.NewLine + e.Message);
            }
        }
        public override string GetResult()
        {
            try
            {
                if (!isOk)
                {
                    return "";
                }
                float A = Factorial(N) / Factorial(N - M);
                return "A = " + A;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка при нахождении размещения" + Environment.NewLine + e.Message);
                return "A != ";
            }
        }
    }
}
