﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Combinatorics
{
    public abstract class Combinatoric
    {
        public int N;
        public int M;
        protected bool isOk;

        public abstract void Init(string n, string m);
        public abstract string GetResult();

        public virtual float Factorial(float num)
        {
            if (num <= 1)
                return 1;
            else
                return num * Factorial(num - 1);
        }
    }
}
