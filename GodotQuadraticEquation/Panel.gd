extends Panel

var A = 0
var B = 0
var C = 0
var isOk = false
var Equation = ""

func GetEquation():
	return Equation + "=0"

func CalcResult():
	print(isOk)
	
	print(str(A) + " " + str(B) + " " + str(C))
	if A == 0 || !isOk:
		return "Invalid expression"
	var D = B * B - 4 * A * C
	if D < 0:
		return "No valid roots"
	elif D == 0:
		var x = (-B + sqrt(D)) / (2 * A)
		if x == 0:
			x = int(x)
		return "The expression has a root = " + str(x)
	else:
		var x1 = (-B + sqrt(D)) / (2 * A)
		if x1 == 0:
			x1 = int(x1)		
		var x2 = (-B - sqrt(D)) / (2 * A)
		if x2 == 0:
			x2 = int(x2)
		return "The expression has 2 roots: x1 = " + str(x1) + ", x2 = " + str(x2)

func Parse(equation):
	if equation.find("=",0) != -1:
		equation = equation.substr(0,equation.find("=",0))
	equation = equation.replace(" ", "").to_lower()
	Equation = equation
	equation += "="	
	print(equation)
	A = 0
	B = 0
	C = 0
	var x2 = false
	var x = false
	isOk = true
	var regex = RegEx.new()
	regex.compile(@"[0-9]+")
	var regexC = RegEx.new()
	regexC.compile(@"[+-/*=]+")
	var i = 0
	while i < equation.length() - 1:
		if (i + 2 < equation.length()):
			if (equation[i] == 'x' && !(equation[i + 1] == '2' && regexC.search(str(equation[i + 2])) != null || regexC.search(str(equation[i + 1])) != null)):
				isOk = false
				return
		if (str(equation[i]) + str(equation[i + 1])) == "x2":
			var count = 0
			if (x2):
				isOk = false
				return
			x2 = true
			var j = i-1
			while j >= 0:
				var Match = regex.search(str(equation[j]))
				if Match != null:
					count += 1
				else:
					break
				j-=1
			if count == 0:
				if i - 1 >= 0:
					if equation[i - 1] == '-':
						A = -1
					else:
						A = 1
				else:
					A = 1
			elif (i - count - 1 >= 0):
				if (equation[i - count - 1] == '-'):
					A = -float(equation.substr(i - count, count))
				else:
					A = float(equation.substr(i - count, count))
			else:
				A = float(equation.substr(i - count, count))
			i+=1
		elif equation[i] == 'x':
			if (x):
				isOk = false
				return
			x = true
			var count = 0
			var j = i - 1
			while j >= 0:
				var Match = regex.search(str(equation[j]))
				if Match != null:
					count+=1
				else:
					break
				j-=1
			print(count)
			if count == 0:
				if i - 1 >= 0:
					if equation[i - 1] == '-':
						B = -1
					else:
						B = 1
				else:
					B = 1
			elif (i - count - 1 >= 0):
				if (equation[i - count - 1] == '-'):
					B = -float(equation.substr(i - count, count))
				else:
					B = float(equation.substr(i - count, count))
			else:
				B = float(equation.substr(i - count, count))
		elif regex.search(str(equation[i])) != null:
			print(str(equation[i]) + "regex")	
			var isSkip = false
			for j in range(i, equation.length()):
				if(equation[j] != 'x'):
					if (regexC.search(str(equation[j])) != null):
						break;
				else:						
					isSkip = true
					break;
			if !isSkip:				
				var count = 0
				for j in range(i, equation.length()):
					var Match = regex.search(str(equation[j]))
					if (Match != null):
						count+=1
					else:
						break
				if (i - 1 >= 0):						
					if (equation[i - 1] == '-'):
						C = -float(equation.substr(i, count))
						print(str(A) + " " + str(B) + " " + str(C))
					else:
						C = float(equation.substr(i, count))
						print(str(A) + " " + str(B) + " " + str(C))
				else:
					C = float(equation.substr(i, count))
					print(str(A) + " " + str(B) + " " + str(C))
				i += count;
		i+=1
	if(!x2):
		return

func _on_GetResult_pressed():
	var time_before = OS.get_ticks_usec()
	Parse($Input/equation.text)
	var output = CalcResult()
	print(output)
	$Result/ResultBox.text = CalcResult()
	var total_time = (OS.get_ticks_usec() - time_before) / 1000.0 / 1000.0
	print(total_time)
	$AcceptDialog.dialog_text = str(total_time)
	$AcceptDialog.popup()

func _on_Load_pressed():
	$OpenFileDialog.popup_centered_ratio(0.9)

func _on_Save_pressed():	
	$SaveFileDialog.popup_centered_ratio(0.9)


func _on_OpenFileDialog_file_selected(path):
	var file = File.new()
	file.open(path,1)
	var eq = ""
	eq = file.get_as_text()
	file.close()
	if eq.find("=",0) != -1:
		eq = eq.substr(0,eq.find("=",0))
	eq = eq.replace(" ", "").to_lower();
	$Input/equation.text = eq
	Parse(eq)	
	$Result/ResultBox.text = CalcResult()

func _on_SaveFileDialog_file_selected(path):
	var output = CalcResult()
	Parse($Input/equation.text)
	$Result/ResultBox.text = output
	var file = File.new()
	file.open(path,2)
	file.store_string(GetEquation() + "\r\n" + output)
	file.close()
