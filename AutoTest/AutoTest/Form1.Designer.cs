﻿namespace AutoTest
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabCombinatorics = new System.Windows.Forms.TabPage();
            this.tabQuadratic = new System.Windows.Forms.TabPage();
            this.tabCubic = new System.Windows.Forms.TabPage();
            this.btnTestComb = new System.Windows.Forms.Button();
            this.btnTestQuad = new System.Windows.Forms.Button();
            this.btnTestCub = new System.Windows.Forms.Button();
            this.BoxM = new System.Windows.Forms.TextBox();
            this.BoxN = new System.Windows.Forms.TextBox();
            this.BoxComb = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.BoxQ = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.BoxC = new System.Windows.Forms.TextBox();
            this.BoxQEq = new System.Windows.Forms.TextBox();
            this.BoxCEq = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabCombinatorics.SuspendLayout();
            this.tabQuadratic.SuspendLayout();
            this.tabCubic.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabCombinatorics);
            this.tabControl1.Controls.Add(this.tabQuadratic);
            this.tabControl1.Controls.Add(this.tabCubic);
            this.tabControl1.Location = new System.Drawing.Point(0, -1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(410, 269);
            this.tabControl1.TabIndex = 0;
            // 
            // tabCombinatorics
            // 
            this.tabCombinatorics.Controls.Add(this.label3);
            this.tabCombinatorics.Controls.Add(this.label2);
            this.tabCombinatorics.Controls.Add(this.label1);
            this.tabCombinatorics.Controls.Add(this.BoxComb);
            this.tabCombinatorics.Controls.Add(this.BoxN);
            this.tabCombinatorics.Controls.Add(this.BoxM);
            this.tabCombinatorics.Controls.Add(this.btnTestComb);
            this.tabCombinatorics.Location = new System.Drawing.Point(4, 22);
            this.tabCombinatorics.Name = "tabCombinatorics";
            this.tabCombinatorics.Padding = new System.Windows.Forms.Padding(3);
            this.tabCombinatorics.Size = new System.Drawing.Size(402, 243);
            this.tabCombinatorics.TabIndex = 0;
            this.tabCombinatorics.Text = "Combinatorics";
            this.tabCombinatorics.UseVisualStyleBackColor = true;
            // 
            // tabQuadratic
            // 
            this.tabQuadratic.Controls.Add(this.label6);
            this.tabQuadratic.Controls.Add(this.BoxQEq);
            this.tabQuadratic.Controls.Add(this.label4);
            this.tabQuadratic.Controls.Add(this.BoxQ);
            this.tabQuadratic.Controls.Add(this.btnTestQuad);
            this.tabQuadratic.Location = new System.Drawing.Point(4, 22);
            this.tabQuadratic.Name = "tabQuadratic";
            this.tabQuadratic.Padding = new System.Windows.Forms.Padding(3);
            this.tabQuadratic.Size = new System.Drawing.Size(402, 243);
            this.tabQuadratic.TabIndex = 1;
            this.tabQuadratic.Text = "Quadratic";
            this.tabQuadratic.UseVisualStyleBackColor = true;
            // 
            // tabCubic
            // 
            this.tabCubic.Controls.Add(this.label7);
            this.tabCubic.Controls.Add(this.BoxCEq);
            this.tabCubic.Controls.Add(this.label5);
            this.tabCubic.Controls.Add(this.BoxC);
            this.tabCubic.Controls.Add(this.btnTestCub);
            this.tabCubic.Location = new System.Drawing.Point(4, 22);
            this.tabCubic.Name = "tabCubic";
            this.tabCubic.Size = new System.Drawing.Size(402, 243);
            this.tabCubic.TabIndex = 2;
            this.tabCubic.Text = "Cubic";
            this.tabCubic.UseVisualStyleBackColor = true;
            // 
            // btnTestComb
            // 
            this.btnTestComb.Location = new System.Drawing.Point(111, 200);
            this.btnTestComb.Name = "btnTestComb";
            this.btnTestComb.Size = new System.Drawing.Size(182, 31);
            this.btnTestComb.TabIndex = 0;
            this.btnTestComb.Text = "Test";
            this.btnTestComb.UseVisualStyleBackColor = true;
            this.btnTestComb.Click += new System.EventHandler(this.btnTestComb_Click);
            // 
            // btnTestQuad
            // 
            this.btnTestQuad.Location = new System.Drawing.Point(111, 200);
            this.btnTestQuad.Name = "btnTestQuad";
            this.btnTestQuad.Size = new System.Drawing.Size(182, 31);
            this.btnTestQuad.TabIndex = 1;
            this.btnTestQuad.Text = "Test";
            this.btnTestQuad.UseVisualStyleBackColor = true;
            this.btnTestQuad.Click += new System.EventHandler(this.btnTestQuad_Click);
            // 
            // btnTestCub
            // 
            this.btnTestCub.Location = new System.Drawing.Point(111, 200);
            this.btnTestCub.Name = "btnTestCub";
            this.btnTestCub.Size = new System.Drawing.Size(182, 31);
            this.btnTestCub.TabIndex = 1;
            this.btnTestCub.Text = "Test";
            this.btnTestCub.UseVisualStyleBackColor = true;
            this.btnTestCub.Click += new System.EventHandler(this.btnTestCub_Click);
            // 
            // BoxM
            // 
            this.BoxM.Location = new System.Drawing.Point(144, 39);
            this.BoxM.Name = "BoxM";
            this.BoxM.ReadOnly = true;
            this.BoxM.Size = new System.Drawing.Size(55, 20);
            this.BoxM.TabIndex = 1;
            // 
            // BoxN
            // 
            this.BoxN.Location = new System.Drawing.Point(45, 39);
            this.BoxN.Name = "BoxN";
            this.BoxN.ReadOnly = true;
            this.BoxN.Size = new System.Drawing.Size(55, 20);
            this.BoxN.TabIndex = 2;
            // 
            // BoxComb
            // 
            this.BoxComb.Location = new System.Drawing.Point(27, 120);
            this.BoxComb.Name = "BoxComb";
            this.BoxComb.ReadOnly = true;
            this.BoxComb.Size = new System.Drawing.Size(351, 20);
            this.BoxComb.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(15, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "N";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(122, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "M";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 102);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Result";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Result";
            // 
            // BoxQ
            // 
            this.BoxQ.Location = new System.Drawing.Point(27, 120);
            this.BoxQ.Multiline = true;
            this.BoxQ.Name = "BoxQ";
            this.BoxQ.ReadOnly = true;
            this.BoxQ.Size = new System.Drawing.Size(351, 74);
            this.BoxQ.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Result";
            // 
            // BoxC
            // 
            this.BoxC.Location = new System.Drawing.Point(27, 120);
            this.BoxC.Multiline = true;
            this.BoxC.Name = "BoxC";
            this.BoxC.ReadOnly = true;
            this.BoxC.Size = new System.Drawing.Size(351, 74);
            this.BoxC.TabIndex = 7;
            // 
            // BoxQEq
            // 
            this.BoxQEq.Location = new System.Drawing.Point(27, 47);
            this.BoxQEq.Name = "BoxQEq";
            this.BoxQEq.ReadOnly = true;
            this.BoxQEq.Size = new System.Drawing.Size(351, 20);
            this.BoxQEq.TabIndex = 9;
            // 
            // BoxCEq
            // 
            this.BoxCEq.Location = new System.Drawing.Point(27, 47);
            this.BoxCEq.Name = "BoxCEq";
            this.BoxCEq.ReadOnly = true;
            this.BoxCEq.Size = new System.Drawing.Size(351, 20);
            this.BoxCEq.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Equation";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Equation";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(412, 264);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabCombinatorics.ResumeLayout(false);
            this.tabCombinatorics.PerformLayout();
            this.tabQuadratic.ResumeLayout(false);
            this.tabQuadratic.PerformLayout();
            this.tabCubic.ResumeLayout(false);
            this.tabCubic.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabCombinatorics;
        private System.Windows.Forms.TabPage tabQuadratic;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox BoxComb;
        private System.Windows.Forms.TextBox BoxN;
        private System.Windows.Forms.TextBox BoxM;
        private System.Windows.Forms.Button btnTestComb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox BoxQEq;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox BoxQ;
        private System.Windows.Forms.Button btnTestQuad;
        private System.Windows.Forms.TabPage tabCubic;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox BoxCEq;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox BoxC;
        private System.Windows.Forms.Button btnTestCub;
    }
}

