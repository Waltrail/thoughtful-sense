﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoTest
{
    public class Combinatorics
    {
        public Combinatorics(string n, string m, string result)
        {
            N = n;
            M = m;
            Result = result;
        }
        public string N;
        public string M;
        public string Result;
    }
    public class Equation
    {
        public Equation(string eq, string result)
        {
            equation = eq;
            Result = result;
        }
        public string equation;
        public string Result;
    }
}
