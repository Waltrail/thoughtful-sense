﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoTest
{
    public partial class Form1 : Form
    {
        public List<Combinatorics> combinatorics;
        public List<Equation> quadratics;
        public List<Equation> cubics;
        public Random random;
        public Form1()
        {
            InitializeComponent();
            random = new Random();
            combinatorics = new List<Combinatorics>()
            {
                new Combinatorics("3","2", "P = 6, A = 6, C = 3"),
                new Combinatorics("6","3", "P = 720, A = 120, C = 20"),
                new Combinatorics("7","1", "P = 5040, A = 7, C = 7"),
                new Combinatorics("3","3", "P = 6, A = 6, C = 1"),
                new Combinatorics("4","3", "P = 24, A = 24, C = 4"),
            };
            quadratics = new List<Equation>()
            {
                new Equation("-3x2=0", "x=0"),
                new Equation("9x2+7=0", "Нет корней"),
                new Equation("-x2+36=0", "x1=6, x2=-6"),
                new Equation("5x2-6x-32=0", "х1 = 3.2, x2 = -2")
            };
            cubics = new List<Equation>()
            {
                new Equation("2x3-11x2+12x+9=0", "x1=3, x2=3, x3=0.5"),
                new Equation("x3+x=0", "x1=0, x2 = 0 | -1, x3 = 0 | 1"),
            };
        }

        private void btnTestComb_Click(object sender, EventArgs e)
        {
            Combinatorics comb = combinatorics[random.Next(0, combinatorics.Count)];
            BoxN.Text = comb.N;
            BoxM.Text = comb.M;
            BoxComb.Text = comb.Result;
        }

        private void btnTestQuad_Click(object sender, EventArgs e)
        {
            Equation eq = quadratics[random.Next(0, quadratics.Count)];
            BoxQEq.Text = eq.equation;
            BoxQ.Text = eq.Result;
        }

        private void btnTestCub_Click(object sender, EventArgs e)
        {
            Equation eq = cubics[random.Next(0, cubics.Count)];
            BoxCEq.Text = eq.equation;
            BoxC.Text = eq.Result;
        }
    }
}
