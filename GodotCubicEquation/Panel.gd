extends Panel

var A = 0
var B = 0
var C = 0
var D = 0
var Real
var Imaginary
var isOk = false
var Equation = ""

func Magnitude():
	if (Real == INF || Imaginary == INF):
		return INF
	var c = abs(Real)
	var d = abs(Imaginary)
	if(c > d):
		var r = d / c
		return c * sqrt(1.0 + r * r)
	elif d == 0.0:
		return c
	else:
		var r = c / d
		return d * sqrt(1.0 + r * r)
		
func Magnitude2(real, imaginary):
	if (real == INF || imaginary == INF):
		return INF
	var c = abs(real)
	var d = abs(imaginary)
	if(c > d):
		var r = d / c
		return c * sqrt(1.0 + r * r)
	elif d == 0.0:
		return c
	else:
		var r = c / d
		return d * sqrt(1.0 + r * r)
		
func Phase():
	return atan2(Imaginary, Real)

func FromPolarCoordinates(magnitude, phase):
	return {"Real": magnitude * cos(phase), "Imaginary": magnitude * sin(phase)}

func ComplexSQRT(real, imaginary):
	return FromPolarCoordinates(sqrt(Magnitude2(real, imaginary)), atan2(imaginary, real) / 2.0)

func Round(num, digit):
    return round(num * pow(10.0, digit)) / pow(10.0, digit) #NtCheck
	
func ToString(real, imaginary):
	imaginary = Round(imaginary, 10)
	real = Round(real, 10)	
	if(imaginary == 0):
		return "Real: " + str(real)
	return "Real: " + str(real) + ", Imaginary: "+ str(imaginary)

func CubicRoots(magnitude, phase):
	var r = pow(magnitude, 1.0 / 3.0)
	var theta = phase / 3
	var shift = PI * 2 / 3
	return {"x1": FromPolarCoordinates(r,theta),
			"x2": FromPolarCoordinates(r,theta + shift),
			"x3": FromPolarCoordinates(r,theta - shift)}

func Divine(real1, imaginary1, real2, imaginary2):
	if(abs(imaginary2) < abs(real2)):
		var doc = imaginary2 / real2
		return {"Real": (real1 + imaginary1 * doc) / (real2 + imaginary2 * doc), 
				"Imaginary": (imaginary1 - real1 * doc) / (real2 + imaginary2 * doc)}
	else:
		var cod = real2 / imaginary2
		return {"Real": (imaginary1 + real1 * cod) / (imaginary2 + real2 * cod), 
				"Imaginary": (-real1 + imaginary1 * cod) / (imaginary2 + real2 * cod)}

func Multiply(real1, imaginary1, real2, imaginary2):
	var realpart = (real1 * real2) - (imaginary1 * imaginary2)
	var imaginarypart = (imaginary1 * real2) + (real1 * imaginary2)
	return {"Real": realpart, "Imaginary": imaginarypart}

func FindRoots():
	var a = B * B - 3.0 * A * C
	var b = 2.0 * B * B * B - 9 * A * B * C + 27.0 * A * A * D
	var s = -1.0 / (3.0 * A)
	var d = (b * b - 4.0 * a * a * a) / (-27.0 * A * A)
	if d == 0.0:
		if a == 0.0:
			return {"x1": {"Real": s * B, "Imaginary": 0.0},
					"x2": {"Real": s * B, "Imaginary": 0.0},
					"x3": {"Real": s * B, "Imaginary": 0.0}}
		var v = {"Real": (9 * A * D - B * C) / (2 * a), "Imaginary": 0.0 }
		var w = {"Real": (4 * A * B * C - 9 * A * A * D - B * B * B) / (A * a), "Imaginary": 0.0 }		
		return {"x1": v, "x2": v, "x3": w}
	var c
	if a == 0:
		c = CubicRoots(Magnitude2(b,0.0),atan2(0.0, b))
	else:
		var complex = ComplexSQRT(b * b - 4 * a * a * a, 0.0)
		complex["Real"] += b;
		complex = Divine(complex["Real"],complex["Imaginary"],2.0, 0.0)		
		c = CubicRoots(Magnitude2(complex["Real"],complex["Imaginary"]),atan2(complex["Imaginary"], complex["Real"]))
		
	var complex1 = Divine(a,0.0,c["x1"]["Real"],c["x1"]["Imaginary"])	
	complex1 = {"Real": B + c["x1"]["Real"] + complex1["Real"],
				"Imaginary": c["x1"]["Imaginary"] + complex1["Imaginary"]}
	complex1 = Multiply(s,0.0,complex1["Real"],complex1["Imaginary"]) 
	
	var complex2 = Divine(a,0.0,c["x2"]["Real"],c["x2"]["Imaginary"])
	complex2 = {"Real": B + c["x2"]["Real"] + complex2["Real"],
				"Imaginary": c["x2"]["Imaginary"] + complex2["Imaginary"]}
	complex2 = Multiply(s,0.0,complex2["Real"],complex2["Imaginary"]) 
	
	var complex3 = Divine(a,0.0,c["x3"]["Real"],c["x3"]["Imaginary"])
	complex3 = {"Real": B + c["x3"]["Real"] + complex3["Real"],
				"Imaginary": c["x3"]["Imaginary"] + complex3["Imaginary"]}
	complex3 = Multiply(s,0.0,complex3["Real"],complex3["Imaginary"]) 
	
	return {"x1": complex1,
			"x2": complex2,
			"x3": complex3}
			
func GetEquation():
	return Equation + "=0"

func CalcResult():	
	if A == 0 || !isOk:
		return "Invalid expression"
	var roots = FindRoots()
	return "x1 = " + ToString(roots["x1"]["Real"],roots["x1"]["Imaginary"]) + "\r\n" + "x2 = " + ToString(roots["x2"]["Real"],roots["x2"]["Imaginary"]) + "\r\n" + "x3 = " + ToString(roots["x3"]["Real"],roots["x3"]["Imaginary"])
	
func Parse(equation):
	if equation.find("=",0) != -1:
		equation = equation.substr(0,equation.find("=",0))
	equation = equation.replace(" ", "").to_lower()
	Equation = equation
	equation += "="
	A = 0
	B = 0
	C = 0
	var x3 = false	
	var x2 = false
	var x = false
	isOk = true
	var regex = RegEx.new()
	regex.compile(@"[0-9]+")
	var regexC = RegEx.new()
	regexC.compile(@"[+-/*=]+")
	var i = 0
	while i < equation.length() - 1:
		if (i + 2 < equation.length()):
			if (equation[i] == 'x' 
			&& !((equation[i + 1] == '2' 
			&& regexC.search(str(equation[i + 2])) != null 
			|| regexC.search(str(equation[i + 1])) != null )
			|| (equation[i + 1] == '3' && regexC.search(str(equation[i + 2])) != null
			|| regexC.search(str(equation[i + 1])) != null ))):
				isOk = false
				return
		if str(equation[i]) + str(equation[i+1]) == "x3":
			var count = 0
			if x3:
				isOk = false
				return
			x3 = true
			var j = i - 1
			while j >= 0:
				var Match = regex.search(str(equation[j]))
				if Match != null:
					count += 1
				else:
					break
				j-=1
			if count == 0:
				if i - 1 >= 0:
					if equation[i - 1] == '-':
						A = -1
					else:
						A = 1
				else:
					A = 1
			elif i - count - 1 >= 0:
				if equation[i - count - 1] == '-':
					A = -float(equation.substr(i-count,count))
				else:
					A = float(equation.substr(i-count,count))
			else:
				A = float(equation.substr(i-count,count))
			i+=1
		elif (str(equation[i]) + str(equation[i + 1])) == "x2":
			var count = 0
			if (x2):
				isOk = false
				return
			x2 = true
			var j = i-1
			while j >= 0:
				var Match = regex.search(str(equation[j]))
				if Match != null:
					count += 1
				else:
					break
				j-=1
			if count == 0:
				if i - 1 >= 0:
					if equation[i - 1] == '-':
						B = -1
					else:
						B = 1
				else:
					B = 1
			elif (i - count - 1 >= 0):
				if (equation[i - count - 1] == '-'):
					B = -float(equation.substr(i - count, count))
				else:
					B = float(equation.substr(i - count, count))
			else:
				B = float(equation.substr(i - count, count))
			i+=1
		elif equation[i] == 'x':
			if (x):
				isOk = false
				return
			x = true
			var count = 0
			var j = i - 1
			while j >= 0:
				var Match = regex.search(str(equation[j]))
				if Match != null:
					count+=1
				else:
					break
				j-=1
			if count == 0:
				if i - 1 >= 0:
					if equation[i - 1] == '-':
						C = -1
					else:
						C = 1
				else:
					C = 1
			elif (i - count - 1 >= 0):
				if (equation[i - count - 1] == '-'):
					C = -float(equation.substr(i - count, count))
				else:
					C = float(equation.substr(i - count, count))
			else:
				C = float(equation.substr(i - count, count))
		elif regex.search(str(equation[i])) != null:
			var isSkip = false
			for j in range(i, equation.length()):
				if(equation[j] != 'x'):
					if (regexC.search(str(equation[j])) != null):
						break;
				else:						
					isSkip = true
					break;
			if !isSkip:				
				var count = 0
				for j in range(i, equation.length()):
					var Match = regex.search(str(equation[j]))
					if (Match != null):
						count+=1
					else:
						break
				if (i - 1 >= 0):						
					if (equation[i - 1] == '-'):
						D = -float(equation.substr(i, count))
					else:
						D = float(equation.substr(i, count))
				else:
					D = float(equation.substr(i, count))
				i += count;
		i+=1
	if(!x2):
		return

func _on_GetResult_pressed():
	var time_before = OS.get_ticks_usec()
	Parse($Input/equation.text)
	var output = CalcResult()
	$Result/ResultBox.text = CalcResult()
	var total_time = (OS.get_ticks_usec() - time_before) / 1000.0 / 1000.0
	$AcceptDialog.dialog_text = str(total_time)
	$AcceptDialog.popup()

func _on_Load_pressed():
	$OpenFileDialog.popup_centered_ratio(0.9)

func _on_Save_pressed():	
	$SaveFileDialog.popup_centered_ratio(0.9)


func _on_OpenFileDialog_file_selected(path):
	var file = File.new()
	file.open(path,1)
	var eq = ""
	eq = file.get_as_text()
	file.close()
	if eq.find("=",0) != -1:
		eq = eq.substr(0,eq.find("=",0))
	eq = eq.replace(" ", "").to_lower();
	$Input/equation.text = eq
	Parse(eq)	
	$Result/ResultBox.text = CalcResult()

func _on_SaveFileDialog_file_selected(path):
	var output = CalcResult()
	Parse($Input/equation.text)
	$Result/ResultBox.text = output
	var file = File.new()
	file.open(path,2)
	file.store_string(GetEquation() + "\r\n" + output)
	file.close()
