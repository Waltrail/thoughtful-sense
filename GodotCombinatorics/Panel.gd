extends Panel

var IsCorrect = false;

var N = - 1;
var M = - 1;

func Factorial(num):
	if num <= 1:
    	return 1
	return num * Factorial(num - 1)

func CalcCombinatorics():
	var C = Factorial(N) / (Factorial(N - M) * Factorial(M))
	return "C = " + str(C)

func CalcAllocation():
	var A = Factorial(N) / Factorial(N - M)
	return "A = " + str(A)

func CalcPermutations():
	var P = Factorial(N)
	return "P = " + str(P)

func _on_Load_pressed():
	$OpenFileDialog.popup_centered_ratio(0.9)
	pass # Replace with function body.


func _on_Save_pressed():
	$SaveFileDialog.popup_centered_ratio(0.9)
	pass # Replace with function body.


func _on_GetResult_pressed():
	var time_before = OS.get_ticks_usec()
	var output = ""
	if IsCorrect:
		output += CalcPermutations() + " "
		output += CalcAllocation() + " "
		output += CalcCombinatorics()
	else:
		output = "Intering uncorrect numbers"
	$Result/ResultBox.text = output	
	var total_time = (OS.get_ticks_usec() - time_before) / 1000.0 / 1000.0
	$AcceptDialog.dialog_text = str(total_time)
	$AcceptDialog.popup()

func _on_LineN_text_changed(new_text):
	print(new_text);
	print(int(new_text));
	if int(new_text) >= 0:
		N = int(new_text)
		if M >= 0:
			IsCorrect = true
	else:
		N = -1
		IsCorrect = false

func _on_LineM_text_changed(new_text):
	print(new_text);
	print(int(new_text));
	if int(new_text) >= 0:
		M = int(new_text)
		if N >= 0:
			IsCorrect = true
	else:
		M = -1
		IsCorrect = false


func _on_OpenFileDialog_file_selected(path):
	var file = File.new()
	file.open(path,1)
	var combin = ""
	combin = file.get_as_text()
	file.close()
	if combin.find("\n",0) != -1:
		combin = combin.substr(0,combin.find("\n",0))
	combin = combin.replace(" ", "").to_lower();
	for i in range(combin.length()): 
		if combin[i] == 'n':
			var count = 0
			for j in range(combin.length()):
				if combin[j] == 'm':
					break;
				else:
					count += 1
			N = int(combin.substr(i + 1, count))
			i += count;
		elif combin[i] == 'm':			
			var count = 0;
			for j in range(i + 1, combin.length()):
				if combin[j] == 'n':
					break
				else:
					count+=1			
			M = int(combin.substr(i + 1, count));
			i += count;
	IsCorrect = M >= 0 && N >= 0
	$Input/LineN.text = str(N);
	$Input/LineM.text = str(M);
	var output = "";		
	if IsCorrect:
		output += CalcPermutations() + " "
		output += CalcAllocation() + " "
		output += CalcCombinatorics()
	else:
		output = "Intering uncorrect numbers"
	$Result/ResultBox.text = output


func _on_SaveFileDialog_file_selected(path):
	var output = ""
	if IsCorrect:
		output += CalcPermutations() + " "
		output += CalcAllocation() + " "
		output += CalcCombinatorics()
	else:
		output = "Intering uncorrect numbers"
		
	$Result/ResultBox.text = output
	var file = File.new()	
	file.open(path,2)
	file.store_string("N" + str(N) + " M" + str(M) + "\r\n" + output)
	file.close()
	
