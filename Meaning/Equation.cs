﻿using System;
using System.Text.RegularExpressions;
using System.Windows;

namespace Meaning
{
    public class Equation
    {
        private string cequation;
        private bool x2;
        private bool x;
        private bool isOk;

        public float A;
        public float B;
        public float C;
        
        public string CEquation
        {
            set { cequation = value; }
            get { return cequation + "=0"; }
        }

        public Equation()
        {
            A = 0;
            B = 0;
            C = 0;
        }
        public void Parse(string equation)
        {
            equation = equation.Replace(" ", "").ToLower();
            CEquation = string.Copy(equation);
            equation += "=";

            try
            {
                A = 0; B = 0; C = 0;
                x2 = false; x = false;
                isOk = true;
                Regex regex = new Regex(@"[0-9]+");
                Regex regexC = new Regex(@"[+-/*=]+");       // x233+3x34-30x30
                for (int i = 0; i < equation.Length - 1; i++)
                {
                    if (i + 2 < equation.Length)
                    {
                        if (equation[i] == 'x'
                            && !(equation[i + 1] == '2' && regexC.Match(equation[i + 2].ToString()).Success
                            || regexC.Match(equation[i + 1].ToString()).Success))
                        {
                            throw new Exception("Given invalid X");
                        }
                    }
                    if (equation[i].ToString() + equation[i + 1].ToString() == "x2")
                    {
                        int count = 0;
                        if (x2)
                        {
                            throw new Exception("X2 meets the second time");
                        }
                        x2 = true;
                        for (int j = i - 1; j >= 0; j--)
                        {
                            Match match = regex.Match(equation[j].ToString());
                            if (match.Success)
                            {
                                count++;
                            }
                            else
                                break;
                        }
                        if(count == 0)
                        {
                            if (i - 1 >= 0)
                            {
                                if (equation[i - count - 1] == '-')
                                {
                                    A = -1;
                                }
                                else
                                    A = 1;
                            }
                            else
                                A = 1;
                        }
                        else if (i - count - 1 >= 0)
                        {
                            if (equation[i - count - 1] == '-')
                            {
                                A = -float.Parse(equation.Substring(i - count, count));
                            }
                            else
                            {
                                A = float.Parse(equation.Substring(i - count, count));
                            }
                        }
                        else
                        {
                            A = float.Parse(equation.Substring(i - count, count));
                        }
                        i++;
                    }
                    else if (equation[i] == 'x')
                    {
                        if (x)
                        {
                            throw new Exception("X meets the second time");
                        }
                        x = true;
                        int count = 0;
                        for (int j = i - 1; j >= 0; j--)                           
                        {                                                    
                            Match match = regex.Match(equation[j].ToString());
                            if (match.Success)
                            {
                                count++;
                            }
                            else
                                break;                                         
                        }
                        if (count == 0)
                        {
                            if (i - 1 >= 0)
                            {
                                if (equation[i - count - 1] == '-')
                                {
                                    B = -1;
                                }
                                else
                                    B = 1;
                            }
                            else
                                B = 1;
                        }
                        else if (i - count - 1 >= 0)
                        {
                            if (equation[i - count - 1] == '-')
                            {
                                B = -float.Parse(equation.Substring(i - count, count));
                            }
                            else
                            {
                                B = float.Parse(equation.Substring(i - count, count));
                            }
                        }
                        else
                        {
                            B = float.Parse(equation.Substring(i - count, count));
                        }
                    }
                    else if (regex.Match(equation[i].ToString()).Success)
                    {
                        for (int j = i; j < equation.Length; j++)
                        {
                            if(equation[j] != 'x')
                            {
                                if (regexC.Match(equation[j].ToString()).Success)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                goto endC;
                            }
                            
                        }
                        int count = 0;
                        for (int j = i; j < equation.Length; j++)
                        {
                            Match match = regex.Match(equation[j].ToString());
                            if (match.Success)
                            {
                                count++;
                            }
                            else
                                break;
                        }
                        if (i - 1 >= 0)
                        {
                            if (equation[i - 1] == '-')
                            {
                                C = -float.Parse(equation.Substring(i, count));
                            }
                            else
                            {
                                C = float.Parse(equation.Substring(i, count));
                            }
                        }
                        else
                        {
                            C = float.Parse(equation.Substring(i, count));
                        }
                        i += count;
                    endC:;
                    }
                }
                if(!x2)
                {
                    throw new Exception("X2 are missing");
                }
            }
            catch(Exception e)
            {
                isOk = false;
                MessageBox.Show("Было введино некоректное уровнение owO" + Environment.NewLine + e.Message);
            }
        }

        public string GetResult()
        {
            Console.WriteLine(A + " " + B + " " + C);
            if(A == 0 || !isOk)
            {
                return "Не верное вырожение";
            }
            double D = Math.Pow(B, 2) - 4 * A * C;
            if(D < 0)
            {
                return "Нет действительных корней";
            }
            else if(D == 0)
            {
                float x = (float)(-B + Math.Sqrt(D)) / (2 * A);
                return "Выражение имеет корень = " + x;
            }
            else
            {
                float x1 = (float)(-B + Math.Sqrt(D)) / (2 * A);
                float x2 = (float)(-B - Math.Sqrt(D)) / (2 * A);
                return "Выражение имеет 2 корня: х1 = " + x1 + ", x2 = " + x2;
            }
        }
    }
}
