﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Meaning
{
    public partial class MainWindow : Window
    {
        Equation equation;
        public MainWindow()
        {
            InitializeComponent();
            equation = new Equation();
            BtnQEGetResult.Click += DoStaff;
            BtnQEIn.Click += Load;
            BtnQEOut.Click += Out;
        }

        private void Out(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            if (saveFileDialog.ShowDialog() == true)
                File.WriteAllText(saveFileDialog.FileName, equation.CEquation + Environment.NewLine + equation.GetResult());
        }

        private void Load(object sender, RoutedEventArgs e)
        {
            string Sequation = "";
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog.Multiselect = false;
            if (openFileDialog.ShowDialog() == true)
                Sequation = File.ReadAllText(openFileDialog.FileName);
            if (Sequation.IndexOf('=') != -1)
            {
                Sequation = Sequation.Substring(0, Sequation.IndexOf('='));
            }
            if (Sequation.IndexOf('\n') != -1)
            {
                Sequation = Sequation.Substring(0, Sequation.IndexOf('\n'));
            }
            BoxQE.Text = Sequation;
            equation.Parse(Sequation);
            BoxResult.Text = equation.GetResult();
        }

        private void DoStaff(object sender, RoutedEventArgs e)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            string Sequation = BoxQE.Text;
            if(Sequation.IndexOf('=') != -1)
            {
                Sequation = Sequation.Substring(0, Sequation.IndexOf('='));
            }
            equation.Parse(Sequation);
            BoxResult.Text = equation.GetResult();
            sw.Stop();
            MessageBox.Show((sw.ElapsedMilliseconds / 1000.0).ToString());
        }
    }
}
